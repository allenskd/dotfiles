" David González's .vimrc file
" thehumble.ninja <david [at] thehumble [dot] ninja> 
 
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
" Add or remove your Bundles here:
Plugin 'Shougo/neosnippet.vim'
Plugin 'Shougo/neosnippet-snippets'
Plugin 'tpope/vim-fugitive'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'flazz/vim-colorschemes'
Plugin 'jordwalke/flatlandia'
Plugin 'edsono/vim-matchit'
Plugin 'wlangstroth/vim-racket'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'tpope/vim-surround'
Plugin 'bling/vim-airline'
Plugin 'mattn/emmet-vim'
Plugin 'tpope/vim-markdown'
Plugin 'Raimondi/delimitMate'
Plugin 'joonty/vim-sauce'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-repeat'
Plugin 'groenewege/vim-less'
Plugin 'othree/html5.vim'
Plugin 'thndgonz/HTML-AutoCloseTag' " custom repo fixing a headers problem
Plugin 'Valloric/YouCompleteMe'
Plugin 'morhetz/gruvbox'
Plugin 'rdnetto/YCM-Generator'
" Required:
call vundle#end()    

" Required:
filetype plugin indent on

" Enable syntax
syntax enable
set t_Co=256 " enable 256 color support
" Set colorscheme
colorscheme gruvbox

" Set leader
let mapleader = ","

" Map NerdTree Toggle
map <Leader>t :NERDTreeToggle<CR>

" Syntastic settings
set statusline +=%#warningmsg#
set statusline +=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checkers = ['pylint']

" End Syntastic settings
"

" Emmet configuration

let g:user_emmet_mode='a' 
autocmd FileType html,js,css EmmetInstall
" End of Emmet configuration

" YouCompleteMe configuration

let g:ycm_global_ycm_extra_conf = '/home/david/.vim/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0

" End YouCompleteMe

" delimitMate configuration

let delimitMate_expand_cr = 1
let delimitMate_expand_inside_quotes = 1

" end of delimit configuration

" Automatically set Markdown files with *.md extensions to the right filetype
"
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" Racket support 
"

if has("autocmd")
    au BufReadPost *.rkt, *.rktl set filetype=racket
    au filetype racket set lisp
endif

" Vim core configurations

set laststatus=2

set hidden " leaves the buffers open
set history=500 " More history lines
set encoding=utf-8 " Sometimes helps prevent insanity
set number " No line numbers no dice
set backspace=indent,eol,start
set re=1 " If the regex is valid use for syntax highlighting


" set zsh
set shell=zsh


set incsearch " Find as we type
set hlsearch " Highlight the ocurrence

set noswapfile 
set nobackup
set nowritebackup

" Indentation
"

set autoindent
set smartindent
set smarttab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab

" Custom Functions
"

" Convert a Less file (*.less) to CSS (*.css) automatically if the file 
" is a `less` file.
"
function LessToCss() 
    let current_file = shellescape(expand('%:p'))
    let filename = expand('%:t:r')
    let to_file = g:current_dir . "/assets/css/" . filename . '.css' 
    let command = "silent !lessc " . current_file . " " . to_file
    execute command
endfunction

autocmd BufWritePost,FileWritePost *.less call LessToCss()
" End of LessToCss section
